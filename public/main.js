$(function() {
  var FADE_TIME = 150; // ms
  var TYPING_TIMER_LENGTH = 400; // ms
  var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7',
    '#ff5050', '#006666', '#800000', '#99ccff'
  ];

  var directionsService = new google.maps.DirectionsService();

  // Initialize varibles
  var $window = $(window);
  var $mapCanvas = $('#mapCanvas');

  // CHAT PAGE
  var $chatPage = $('.chat.page'); // The chatroom page
  var $messages = $('.messages'); // Messages area
  var $inputMessage = $('.inputMessage'); // Input message input box

  // LOGIN PAGE
  var $loginPage = $('.login.page'); // The login page
  var $lg_usernameInput = $('.login.page .usernameInput'); // Input for username in login page
  var $lg_passwordInput = $('.login.page .passwordInput'); // Input for password in login page

  // SIGNUP PAGE
  var $signUpPage = $('.signUp.page'); // The sign-up page
  var $su_usernameInput = $('.signUp.page .usernameInput'); // Input for username in sign-up page
  var $su_passwordInput1 = $('.signUp.page input:nth-of-type(2)'); // Input for password in sign-up page
  var $su_passwordInput2 = $('.signUp.page input:nth-of-type(3)'); // Input for password verification

/*
  var lobbyMapOptions = {
        center: {lat: 20, lng: 0},
        zoom: 3,        
        disableDefaultUI: true,
        //draggable: false,
        //scrollwheel: false,
        //disableDoubleClickZoom: true,
        mapTypeId: google.maps.MapTypeId.HYBRID
      };
*/

  // Prompt for setting a username

  // usernames may contain alphanumeric characters and underscores
  var usernamePattern = new RegExp("^[A-Za-z0-9_]*$");

  var player;

  var username; // TODO: replace this with player.username
  var connected = false;
  var typing = false;
  var lastTypingTime;
  $lg_usernameInput.focus();
  // var $currentInput = $usernameInput.focus();

  // Connect to the server!
  var socket = io();

  var game;
  var gameMarkers = [];

  var playerMarkers = {};
  var boundsRectangle = {};
  var bounds = {};

  var gameLoopHandler;
  var UPDATE_FREQ = 100;

  var roadCheckHandler;
  var ROAD_CHECK_FREQ = 1000;

  var gameTimeHandler;

  var controls = [];
  var scores = [];
  var display = false;
  var controlText;
  var controlText2;

  var topCops = [];
  var topRobs = [];

  function gameLoop () {
    move(player);
    returnToBounds(player.pos, bounds);
    playerMarkers[player.username].setPosition(player.pos);
    map.panTo(player.pos);

    var playerPos = playerMarkers[username].getPosition();
    playerPos = {lat: playerPos.lat(), lng: playerPos.lng()}
    socket.emit('updatePos', {
      pos: playerPos,
      team: player.team
    });
    loseSpeed(player);
  }

  function roadCheck () {
    snapPlayerToNearestRoad(player);
  }

  function gameTime () {
    game.time = (game.time - 1);
    controlText.innerHTML = '<b>' + game.time + '<b>'; 
  }

  function emitUp () {
    if (connected) {
      socket.emit('up', username);
    }
  }

  function drawGameOnMap (name, pos) {
    var marker = new google.maps.Marker({
      map: map,
      position: pos,
      title: name,
      animation: google.maps.Animation.BOUNCE,
    });
    gameMarkers.push(marker);
    google.maps.event.addListener(marker,'click',function() {
      socket.emit('joinGame', name);
    });
  }

  // Sets the map on all markers in the array.
  function setAllMap(markers, map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers(markers) {
    setAllMap(markers, null);
  }

  // Shows any markers currently in the array.
  function showMarkers(markers) {
    setAllMap(markers, map);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkers(markers) {
    clearMarkers(markers);
    markers = [];
  }

  function drawPlayerOnMap (username, pos) {
    if (playerMarkers.hasOwnProperty(username)) {
      playerMarkers[username].setPosition(pos);
      return;
    }

    var marker = new google.maps.Marker({
      map: map,
      position: pos,
      title: username,
      animation: google.maps.Animation.DROP,
      draggable: true,
      icon: 'img/walking.gif',
      optimized: false
    });
    playerMarkers[username] = marker;
  }

  function addParticipantsMessage (numPlayers) {
    var message = '';
    if (numPlayers === 1) {
      message += "there's 1 participant";
    } else {
      message += "there are " + numPlayers + " participants";
    }
    log(message);
  }

  function login () {
    var username = cleanInput($lg_usernameInput.val().trim());
    var password = cleanInput($lg_passwordInput.val());

    if (usernamePattern.test(username)) {
      // Pass your credentials to the server
      socket.emit('login', {
        username: username,
        password: password
      });
    } else {
      alert('Usernames may only contain alphanumeric characters and underscores');
    }
  }

  // Register for an account!
  function signUp () {
    var username = cleanInput($su_usernameInput.val().trim());
    var password1 = cleanInput($su_passwordInput1.val());
    var password2 = cleanInput($su_passwordInput2.val());

    var errorMessage = "";

    if (!usernamePattern.test(username)) {
      errorMessage += "Usernames may only contain alphanumeric characters and underscores";
    }
    if (password1 !== password2) {
      errorMessage += "Your passwords don't match!";
    }

    if (!errorMessage) {
      socket.emit('signUp', {
        username: username,
        password: password1
      });
    } else {
      alert(errorMessage);
    }
  }

  // Sends a chat message
  function sendMessage () {
    var message = $inputMessage.val();
    var recipient;
    if (player != null) {
      recipient = '/' + player.team;

      if (strStartsWith(message, '/a ')) {
        message = message.substr(3);
        recipient = '';
      }

    } else {
      recipient = '';
    }

    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      addChatMessage({
        username: (recipient == '' && player != null ? '[all] ' : '') + username,
        message: message
      });
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', {
        message: message,
        recipient: recipient
      });
    }
  }

  function strStartsWith(str, prefix) {
      return str.indexOf(prefix) === 0;
  }

  // Log a message
  function log (message, options) {
    var $el = $('<li>').addClass('log').text(message);
    addMessageElement($el, options);
  }

  // Adds the visual chat message to the message list
  function addChatMessage (data, options) {
    // Don't fade the message in if there is an 'X was typing'
    var $typingMessages = getTypingMessages(data);
    options = options || {};
    if ($typingMessages.length !== 0) {
      options.fade = false;
      $typingMessages.remove();
    }

    var $usernameDiv = $('<span class="username"/>')
      .text(data.username)
      .css('color', getUsernameColor(data.username));
    var $messageBodyDiv = $('<span class="messageBody">')
      .text(data.message);

    var typingClass = data.typing ? 'typing' : '';
    var $messageDiv = $('<li class="message"/>')
      .data('username', data.username)
      .addClass(typingClass)
      .append($usernameDiv, $messageBodyDiv);

    addMessageElement($messageDiv, options);
  }

  // Adds the visual chat message to the message list
  function clearChat () {
    $messages.empty();
  }

  // Adds the visual chat typing message
  function addChatTyping (data) {
    data.typing = true;
    data.message = 'is typing';
    addChatMessage(data);
  }

  // Removes the visual chat typing message
  function removeChatTyping (data) {
    getTypingMessages(data).fadeOut(function () {
      $(this).remove();
    });
  }

  // Adds a message element to the messages and scrolls to the bottom
  // el - The element to add as a message
  // options.fade - If the element should fade-in (default = true)
  // options.prepend - If the element should prepend
  //   all other messages (default = false)
  function addMessageElement (el, options) {
    var $el = $(el);

    // Setup default options
    if (!options) {
      options = {};
    }
    if (typeof options.fade === 'undefined') {
      options.fade = true;
    }
    if (typeof options.prepend === 'undefined') {
      options.prepend = false;
    }

    // Apply options
    if (options.fade) {
      $el.hide().fadeIn(FADE_TIME);
    }
    if (options.prepend) {
      $messages.prepend($el);
    } else {
      $messages.append($el);
    }
    $messages[0].scrollTop = $messages[0].scrollHeight;
  }

  // Prevents input from having injected markup
  function cleanInput (input) {
    return $('<div/>').text(input).text();
  }

  // Updates the typing event
  function updateTyping () {
    if (connected) {
      if (!typing) {
        typing = true;
        socket.emit('typing');
      }
      lastTypingTime = (new Date()).getTime();

      setTimeout(function () {
        var typingTimer = (new Date()).getTime();
        var timeDiff = typingTimer - lastTypingTime;
        if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
          socket.emit('stop typing');
          typing = false;
        }
      }, TYPING_TIMER_LENGTH);
    }
  }

  // Gets the 'X is typing' messages of a user
  function getTypingMessages (data) {
    return $('.typing.message').filter(function (i) {
      return $(this).data('username') === data.username;
    });
  }

  // Gets the color of a username through our hash function
  function getUsernameColor (username) {
    // Compute hash code
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
       hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
  }

  // Keyboard events
  var keys = {};
  $mapCanvas.keydown(function (event) {
    var code = event.keyCode || event.which;

    switch (code) {
      case 27: // Esc
        if (player != null) {
          leaveGame();
          return;
        }        
    }

    keys[code] = true;
    if (keys[37]) { // WEST
      west(player);
    } 
    if (keys[38]) { // NORTH
      north(player);
    }
    if (keys[39]) { // EAST
      east(player)
    }
    if (keys[40]) { // SOUTH
      south(player);
    }
    if (keys[17] && !display) {
      var ControlGlo = document.createElement('glo');
      var centerControl1 = new ghostsControl(ControlGlo, map);
      ControlGlo.index = 2;
      map.controls[google.maps.ControlPosition.TOP_RIGHT].push(ControlGlo);
      scores.push({pos: google.maps.ControlPosition.TOP_RIGHT, control: ControlGlo});
      display = true;
    }
  }).keyup(function (event) {
    var code = event.keyCode || event.which;
    keys[code] = false;
    if (!keys[17] && display) {
      scores.forEach(function (control) {
        map.controls[control.pos].pop(control.control);
      });
      scores = [];
      display = false;
    }
  });

  function leaveGame () {
    clearIntervals();
    socket.emit('leaveGame', player.team);
    for (var username in playerMarkers) {
        if (playerMarkers.hasOwnProperty(username)) {
            var marker = playerMarkers[username];
            marker.setMap(null);
            delete playerMarkers[username];
        }
    }
    cleanBoard ();
    player = null;
    gotoLobby();
  }

  function setIntervals() {
    gameLoopHandler = setInterval(gameLoop, UPDATE_FREQ);
    roadCheckHandler = setInterval(roadCheck, ROAD_CHECK_FREQ);
    setTimeout(function () {
      gameTimeHandler = setInterval(gameTime, 1000);
    }, 1000);
  }

  function clearIntervals() {
    clearInterval(gameLoopHandler);
    clearInterval(roadCheckHandler);
    clearInterval(gameTimeHandler);
  }

  function cleanBoard () {
    boundsRectangle.setMap(null);
    delete boundsRectangle; // delete the bounds rectangle
    delete bounds;
    removeControlElements();
  }

  function addControlElements() {
    var homeControlDiv = document.createElement('div');
    homeControlDiv.style.padding = '5px';
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = 'rgba(0, 0, 0, 0.3)';
    controlUI.style.border = '1px solid black';
    controlUI.style.opacity = '1'
    controlUI.style.textAlign = 'center';
    homeControlDiv.appendChild(controlUI);
    controlText = document.createElement('div');
    controlText.style.fontFamily='Arial,sans-serif';
    controlText.style.color = 'white';
    controlText.style.fontSize= '24px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';
    controlText.innerHTML = '<b>' + game.time + '<b>'
    controlUI.appendChild(controlText);
    map.controls[google.maps.ControlPosition.TOP].push(homeControlDiv);
    controls.push({pos: google.maps.ControlPosition.TOP, control: homeControlDiv});
  }

  function ghostsControl(controlDiv, map) {
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = 'rgba(0, 0, 0, 0.3)';
    controlUI.style.border = '1px solid black';
    controlUI.style.opacity = '1'
    controlUI.style.textAlign = 'center';
    controlDiv.appendChild(controlUI);
    controlText2 = document.createElement('div');
    controlText2.style.fontFamily='Arial,sans-serif';
    controlText2.style.color = 'white';
    controlText2.style.fontSize= '50px';
    controlText2.style.paddingLeft = '4px';
    controlText2.style.paddingRight = '4px';
    controlText2.innerHTML = '<table style="width:100%"><tr><td><b><ins>Robs:</ins></b></td><td></td><td><i><ins>Cops:</ins></i></td><td></td></tr><tr><td><small>Empty</small></td><td><small>0</small></td><td><small>Empty</small></td><td><small>0</small></td></tr><tr><td><small>Empty</small></td><td><small>0</small></td><td><small>Empty</small></td><td><small>0</small></td></tr><tr><td><small>Empty</small></td><td><small>0</small></td><td><small>Empty</small></td><td><small>0</small></td></tr></table>';
    controlUI.appendChild(controlText2);
  }
  
  function updateScoreBoard() {
    var newTable = '<table style="width:100%"><tr><td><tr><td>Robs:</td><td></td><td>Cops:</td><td></td></tr>' + topRobs[0].id + '</td><td>' + topRobs[0].score + '</td><td>' + topCops[0].id + '</td><td>' + topCops[0].score + '</td></tr><tr><td>' + topRobs[1].id + '</td><td>' + topRobs[1].score + '</td><td>' + topCops[1].id + '</td><td>' + topCops[1].score + '</td></tr><tr><td>' + topRobs[2].id + '</td><td>' + topRobs[2].score + '</td><td>' + topCops[2].id + '</td><td>' + topCops[2].score + '</td></tr></table>';
   
    controlText2.innerHTML = newTable;
  }

  function removeControlElements() {
    controls.forEach(function (control) {
      map.controls[control.pos].pop(control.control);
    });
    controls = [];
  }

  // When the client hits ENTER on their keyboard at LOGIN
  $(".loginForm input").keypress(function(event) {
      if (event.which == 13) {
          event.preventDefault();
          login();
      }
  });

  $(".signUpForm input").keypress(function(event) {
      if (event.which == 13) {
          event.preventDefault();
          signUp();
          $su_passwordInput1.val("");
          $su_passwordInput2.val("");
      }
  });

  // When the client hits ENTER on their keyboard in CHAT
  $(".inputMessage").keypress(function(event) {
      if (event.which == 13) {
          event.preventDefault();
          if (username) {
            sendMessage();
            socket.emit('stop typing');
            typing = false;
          }
      }
  });

  $inputMessage.on('input;', function() {
    updateTyping();
  });

  // Click events

  // Focus input on the map when clicking on it
  $mapCanvas.click(function() {
    $mapCanvas.focus();
  });

/*
  // Focus input when clicking anywhere on login page
  $loginPage.click(function () {
    $currentInput.focus();
  });
*/


  // Focus input when clicking anywhere on chat page
  $chatPage.click(function () {
    $inputMessage.focus();
  });


/*
  // Focus input when clicking on the message input's border
  $inputMessage.click(function () {
    $inputMessage.focus();
  });
*/

  // Go to the sign-up page from the login page
  $('.toSignUp').click(function () {
    $loginPage.fadeOut();
    $signUpPage.show();
    $loginPage.off('click');
    $('.signUpForm .usernameInput').focus();
  });

  // Go back to the login page from the sign-up page
  $('.toLogin').click(function () {
    $signUpPage.fadeOut();
    $loginPage.show();
    $signUpPage.off('click');
    $('.loginForm .usernameInput').focus();
  });

  // Socket events

  // notified by server
  socket.on('update', function (data) {
    // draw players
    // TODO: NEED TO PASS IN TEAM
    drawPlayerOnMap(data.username, data.pos);
    //for (var username in data.cops) {
    //  var player = data.cops[username];
    //  drawPlayerOnMap(player.username, player.pos);
    //}
    //for (var username in data.robbers) {
    //  var player = data.robbers[username];
    //  drawPlayerOnMap(player.username, player.pos);
    //}
  });

  // Whenever the server emits 'loginSuccess', log the login message
  socket.on('loginSuccess', function (data) {

    // Record your username
    username = data.username;

    // Replace the login page with the chat page    
    $loginPage.fadeOut();
    $signUpPage.hide();
    $chatPage.show();
    $loginPage.off('click');
    $inputMessage.focus();   

    connected = true;
    gotoLobby(data);
  });


  function gotoLobby(data) {
    // flash the map
    $mapCanvas.fadeOut(300).fadeIn(300);
    map.setOptions({
        center: {lat: 20, lng: 0},
        zoom: 3,        
        disableDefaultUI: true,
        //draggable: false,
        //scrollwheel: false,
        //disableDoubleClickZoom: true,
        mapTypeId: google.maps.MapTypeId.HYBRID
    })

    clearChat();

    // Display the welcome message
    var message = "Welcome to our WebApp!";
    log(message, {
      prepend: true
    });
    findGames();
  }

  function findGames() {
    socket.emit('findGames');
  }

  socket.on('games', function (data) {
    addParticipantsMessage(data.numPlayers);

    for (var name in data.games) {
      var game = data.games[name];
      drawGameOnMap(game.name, game.center);
    }
  })

  socket.on('joinSuccess', function (data) {
    game = data.game;
    player = data.player;
    player.pos = randomPos(data.swBound, data.neBound);

    deleteMarkers(gameMarkers);
    $mapCanvas.fadeOut(300).fadeIn(300);
    map.setOptions({
      center: data.center,
      zoom: 19,
      tilt: 0,
      disableDefaultUI: true,
      //draggable: false,
      //scrollwheel: false,
      //disableDoubleClickZoom: true,
      mapTypeId: google.maps.MapTypeId.HYBRID
    });
    drawGameBounds(data.swBound, data.neBound);
    initBounds(data.swBound, data.neBound);

    clearChat();

    // Display the welcome message
    log(data.message, {
      prepend: true
    });
    addParticipantsMessage(data.numPlayers);
    drawPlayerOnMap(player.username, player.pos);
    // Start submitting data to the server
    addControlElements();
    setIntervals();
  });

  function randomPos (swBound, neBound) {
    var southwest = new google.maps.LatLng(swBound.lat, swBound.lng);
    var northeast = new google.maps.LatLng(neBound.lat, neBound.lng);
    var lngSpan = northeast.lng() - southwest.lng();
    var latSpan = northeast.lat() - southwest.lat();
    //var randPos = new google.maps.LatLng(
    //    southwest.lat() + latSpan * Math.random(),
    //    southwest.lng() + lngSpan * Math.random());

    return {lat: southwest.lat() + latSpan * Math.random(),
            lng: southwest.lng() + lngSpan * Math.random()};
  }

  function snapPlayerToNearestRoad (player) {
    var currPos = new google.maps.LatLng(player.pos.lat, player.pos.lng);
    directionsService.route({
      origin:currPos,
      destination:currPos,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    }, function(response, status) {            
      if (status == google.maps.DirectionsStatus.OK) {
        var marker = playerMarkers[player.username];

        // console.log(response.routes[0].legs[0].start_location.lat);

        marker.setPosition(response.routes[0].legs[0].start_location);
        player.pos = {lat: response.routes[0].legs[0].start_location.lat(),
                      lng: response.routes[0].legs[0].start_location.lng()
        };
      }
    });
  }

  function drawGameBounds (swBound, neBound) {
    boundsRectangle = new google.maps.Rectangle({
      strokeColor: '#ffffff',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#ffffff',
      fillOpacity: 0.1,
      map: map,
      bounds: new google.maps.LatLngBounds(
        new google.maps.LatLng(swBound.lat, swBound.lng),
        new google.maps.LatLng(neBound.lat, neBound.lng))
    });    
    // gameMarkers.push(rectangle);
  }

  function initBounds(swBound, neBound) {
    bounds.south = swBound.lat;
    bounds.west = swBound.lng;
    bounds.north = neBound.lat;
    bounds.east = neBound.lng;
  }

  socket.on('youGotCaught', function (data) {
    alert('You got caught by ' + data.caughtBy);
    log('You got caught by ' + data.caughtBy);
    player.team = 'cops';
  });

  socket.on('player caught', function (data) {
    if (player.team === 'cops') {
      log(data.caught + ' caught ' + data.caughtBy);
    } else {
      log(data.caughtBy + ' got caught by ' + data.caught);      
    }
  });

  socket.on('loginFail', function (message) {
    $lg_passwordInput.val("");
    alert(message);    
  });

  socket.on('signUpSuccess', function (data) {
    $signUpPage.fadeOut();
    $loginPage.show();
    $signUpPage.off('click');
    $su_usernameInput.val("");
    $lg_usernameInput.val(data.username);
    $lg_passwordInput.focus();
    alert(data.message);
  });

  socket.on('signUpFail', function (message) {
    $('signUp.page input').val("");
    alert(message);
  });

  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', function (data) {
    addChatMessage(data);
  });

  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('user joined', function (data) {
    log(data.username + ' joined');
    addParticipantsMessage(data.numPlayers);
  });

  // Whenever the server emits 'user left', remove the player's marker
  // and log it in the chat body
  socket.on('user left', function (data) {
    playerMarkers[data.username].setMap(null);
    delete playerMarkers[data.username];
    log(data.username + ' left');
    addParticipantsMessage(data.numPlayers);
    removeChatTyping(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('typing', function (data) {
    addChatTyping(data);
  });

  // Whenever the server emits 'stop typing', kill the typing message
  socket.on('stop typing', function (data) {
    removeChatTyping(data);
  });

  socket.on('endGame', function () {
    alert('GAME HAS ENDED');
    leaveGame();
  });
});
