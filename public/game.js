var TOP_SPEED = 100;

var HELLO = 2;

var acceleration = 10;
var deceleration = 30;


function move (player) {
  var oldPos = player.pos;
  player.pos = {lat:oldPos.lat + player.ya * 0.0000001, lng:oldPos.lng + player.xa * 0.0000001 * HELLO};
}

function returnToBounds (pos, bounds) {
  if (pos.lng < bounds.west) {pos.lng = bounds.west;}
  else if (pos.lng > bounds.east) {pos.lng = bounds.east;}
  if (pos.lat < bounds.north) {pos.lat = bounds.north;}
  else if (pos.lat > bounds.south) {pos.lat = bounds.south;}
}

function west (player) {
  if (player.xa > 0) {
    // if currently going east
    player.xa -= deceleration;
  }
  else if (player.xa > -TOP_SPEED) {
    player.xa -= acceleration;
  }
}

function north (player) {
  if (player.ya < 0) {
    // if currently going south
    player.ya += deceleration;
  }
  else if (player.ya < TOP_SPEED) {
    player.ya += acceleration;
  }
}

function east (player) {
  if (player.xa < 0) {
    // if currently going west
    player.xa += deceleration;
  }
  if (player.xa < TOP_SPEED) {
    player.xa += acceleration;
  }
}

function south (player) {
  if (player.ya > 0) {
    // if currently going north
    player.ya -= deceleration;
  }
  else if (player.ya > -TOP_SPEED) {
    player.ya -= acceleration;
  }
}

function loseSpeed (player) {
  var speedLoss = 0.95; // change me
  player.xa *= speedLoss;
  player.ya *= speedLoss;
}

function stop (player) {
  player.xa = 0;
  player.ya = 0;
}