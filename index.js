// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var passwordHash = require('password-hash');
var port = process.env.PORT || 3000;

// Setup connection to PostgreSQL database
var pg = require('pg');
var connectionString = 'pg://g1427125_u:htolmv7QYB@db.doc.ic.ac.uk:5432/g1427125_u';

// Listen for clients
server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));

// Game data

var UPDATE_FREQ = 2000;
var COLLISION_DISTANCE = 0.00009; 

var ICL = {lat: 51.498673, lng: -0.179667};
var SAWSTON = {lat: 52.122665, lng: 0.169790};
var TOP_SPEED = 100;

function player(username, team, pos) {
  this.username = username;
  this.team = team;
  this.xa = 0;
  this.ya = 0;
  this.TOP_SPEED = TOP_SPEED;
  this.pos = pos;
  this.img = 'img/walking.gif'
}

function game(name, center, capacity) {
  var latRadius = 0.01;
  var lngRadius = 0.01;

  this.name = name;
  this.swBound = {
    lat: center.lat + latRadius,
    lng: center.lng - lngRadius
  }
  this.neBound = {
    lat: center.lat - latRadius,
    lng: center.lng + lngRadius
  }
  this.center = center;
  this.capacity = capacity;
  this.cops = {};
  this.robbers = {};
  this.time = 300;
  this.countdown = function () {
    var self = this;
    setInterval(function () {
      if (self.time == 0) {
        endGame(self.name);
      }
      self.time = (self.time - 1);
    }, 1000);
  };
}

// Chatroom

// players which are currently connected to the chat
var players = {};
var numPlayers = 0;

var games = {}
games['icl'] = new game('icl', ICL, 2);
games['icl'].countdown();
games['sawston'] = new game('sawston', SAWSTON, 2);
games['sawston'].countdown();

io.on('connection', function (socket) {
  socket.addedUser = false;
  socket.username = '';
  socket.game = '';

  socket.on('updatePos', function (data) {
    var player = games[socket.game][data.team][socket.username];

    if (player == null) {
      player = (data.team === 'cops' ? games[socket.game]['robbers'][socket.username] : games[socket.game]['cops'][socket.username]);
    }

    player.pos = data.pos;
    if (player.team === 'robbers') {convertIfCollided (player);}

    socket.broadcast.to(socket.game).emit('update', {
      username: socket.username,
      team: data.team,
      pos: data.pos
    });
  });

  // TODO: MOVE TO GAME.JS
  function convertIfCollided (player) {
    var game = games[socket.game];
    var opponents = (player.team === 'cops' ? game['robbers'] : game['cops']);

    for (var username in opponents) {
      var opponent = opponents[username];

      if (collided(player.pos, opponent.pos)) {
        if (player.team === 'cops') {
          console.log(player.username + ' in ' + game.name +  ' caught ' + opponent.username);
          convertPlayer(opponent);
          youGotCaught(opponent, player);
        } else {
          console.log(player.username + ' in ' + game.name +  ' caught ' + opponent.username);
          convertPlayer(player);
          youGotCaught(player, opponent);
        }
      }
    }
  }

  function youGotCaught (robber, caughtBy) {
    socket.emit('youGotCaught', {
      caughtBy: caughtBy.username
    });

    socket.leave(socket.game + '/robbers');
    socket.join(socket.game + '/cops');

    socket.broadcast.to(socket.game).emit('player caught', {
      caught: robber.username,
      caughtBy: caughtBy.username
    })
  }

  function convertPlayer (player) {
    var game = games[socket.game];

    var oldTeam = player.team;
    var opposingTeam = (player.team === 'cops' ? 'robbers' : 'cops');    
    player.team = opposingTeam;
    game[opposingTeam][socket.username] = game[oldTeam][socket.username];
    delete game[oldTeam][socket.username];
  }

  function collided (pos1, pos2) {
    return calculateDistance(pos1, pos2) < COLLISION_DISTANCE;
  }

  function calculateDistance(pos1, pos2) {
    // cartesian distance
    return Math.sqrt(Math.pow(pos1.lng - pos2.lng, 2) + Math.pow(pos1.lat - pos2.lat, 2));
  }

  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.to(socket.game + data.recipient).emit('new message', {
      username: (data.recipient == '' && socket.game != 'lobby' ? '[all] ' : '') + socket.username,
      message: data.message
    });
    console.log(socket.username + " in " + socket.game + ": " + data.message);
  });

  socket.on('findGames', function () {
    socket.emit('games', {
      games: games,
      numPlayers: numPlayers
    });
  });

  socket.on('joinGame', function (gameName) {
    socket.leave('lobby');
    socket.join(gameName);
    socket.game = gameName;

    var game = games[socket.game];
    var player = joinATeam(game);
    socket.join(gameName + '/' + player.team);
    var numPlayersInGame = Object.keys(game.cops).length + Object.keys(game.robbers).length;
    
    socket.emit('joinSuccess', {
      player: player,
      game: game,
      center: game.center,
      swBound: game.swBound,
      neBound: game.neBound,
      message: 'Welcome to ' + game.name,
      numPlayers: numPlayersInGame
    });

    socket.broadcast.to(socket.game).emit('user joined', {
      username: socket.username,
      numPlayers: numPlayersInGame
    });
    console.log(socket.username + ' joined the ' + player.team + ' in ' + gameName);
  });

  // returns the created player object
  function joinATeam (game) {
    var numCops = Object.keys(game.cops).length;
    var numRobbers = Object.keys(game.robbers).length;
    if (numCops <= numRobbers) {
      game.cops[socket.username] = new player(socket.username, 'cops', game.center);
      return game.cops[socket.username];
    } else {
      game.robbers[socket.username] = new player(socket.username, 'robbers', game.center);
      return game.robbers[socket.username];
    }
  }

  socket.on('leaveGame', function (team) {
    socket.leave(socket.game);
    socket.leave(socket.game + '/' + team);
    console.log(socket.username + ' left ' + socket.game);

    // remove us from the game
    var game = games[socket.game];
    delete game[team][socket.username];

    var numPlayersInGame = Object.keys(game.cops).length + Object.keys(game.robbers).length;
    socket.broadcast.to(socket.game).emit('user left', {
      username: socket.username,
      numPlayers: numPlayersInGame
    });

    socket.game = 'lobby';
    socket.join('lobby');
  });

  // when the client emits 'login', this listens and executes
  socket.on('login', function (credentials) {
    validateLogin(credentials.username, credentials.password, socket);
  });

  // when the client emits 'signUp', this listens and executes
  socket.on('signUp', function (details) {
    validateSignUp(details.username, details.password, socket);
  });


  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.to(socket.game).emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.to(socket.game).emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    // remove the username from global players list
    if (socket.addedUser) {
      delete players[socket.username];
      --numPlayers;      

      // echo globally that this client has left
      socket.broadcast.to(socket.game).emit('user left', {
        username: socket.username,
        numPlayers: numPlayers
      });
      console.log(socket.username + " disconnected");
    }
  });
});

// HELPER FUNCTIONS

function validateLogin (username, password, socket) {

  // If the user is already in the game:
  if (players.hasOwnProperty(username)) {
    var message = "You're already signed-in!";
    socket.emit('loginFail', message);
    return;
  }

  pg.connect(connectionString, function(err, client, done) {
    client.query('SELECT password FROM users WHERE username = $1', [username], function(err, result) {
      done();

      if (result.rows[0] == undefined) {
        var message = "That user doesn't exist!";
        socket.emit('loginFail', message);
        return;
      }

      // TODO: ENCRYPTION !!!

      // CREDENTIALS OK!
      if (passwordHash.verify(password, result.rows[0].password)) {
        loginSuccess(username, socket);
      } else {
        var message = "Incorrect password!";
        socket.emit('loginFail', message);
      }
    });
  });
}

function loginSuccess (username, socket) {
  // we store the username in the socket session for this client
  socket.username = username;
  socket.game = 'lobby';
  socket.join('lobby');

  // add the client to the global list of current players
  players[username] = new player(username);
  ++numPlayers;
  socket.addedUser = true;  

  socket.emit('loginSuccess', {
    username: username,
  });
  // echo globally (all clients) that a person has connected
  socket.broadcast.to(socket.game).emit('user joined', {
    username: username,
    numPlayers: numPlayers
  });
  console.log(username + " signed in");
}

function validateSignUp (username, password, socket) {
  pg.connect(connectionString, function(err, client, done) {
    client.query('SELECT username FROM users WHERE username = $1', [username], function(err, result) {
      done();
      var message;
      var hashedPassword = passwordHash.generate(password);
      // If this is a new username:
      if (result.rows[0] == undefined) {
        pg.connect(connectionString, function(err, client, done) {
          client.query('INSERT INTO users (username, password) VALUES ($1, $2)', [username, hashedPassword], function(err, result) {
            client.query('SELECT user_id FROM users WHERE username = $1', [username], function(err1, result1) {
              client.query('INSERT INTO stats (user_id, highscore, arrests, total_robberies) VALUES ($1, 0, 0, 0)', [result1.rows[0].user_id], function(err, result) {
                message = "Registration successful!";
                socket.emit('signUpSuccess', {
                  username: username,
                  message: message
                });
              });
            });
          });
        });
        console.log(username + ' just created an account');
      } else {
        message = "Someone with that username already exists!";
        socket.emit('signUpFail', message);
      }
    });
  });
}

function endGame (gameName) {
  io.to(gameName).emit('endGame');
  games[gameName].time = 300;
}
